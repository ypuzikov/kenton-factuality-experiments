#!/bin/bash

resources_url=http://download.joachims.org/svm_rank/current
filename=svm_rank.tar.gz

mkdir svm_rank
cd svm_rank
wget -N $resources_url/$filename
tar -xzvf $filename
rm $filename
make
cd svm_light
make
cd ../..
