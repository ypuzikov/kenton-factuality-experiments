#!/bin/bash

if [ "$#" -ne 1 ]
then
    echo "Usage: $0 <exp_file>"
    exit 1
fi
mvn compile exec:exec -Dexp=$1
