package edu.uw.cs.lil.events.lemma;

import java.util.Set;

public interface ILemmatizer {
	Set<String> lemmatize(String word);

	Set<String> lemmatize(String word, String tag);
}
