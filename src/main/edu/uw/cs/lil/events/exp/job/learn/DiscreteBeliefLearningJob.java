package edu.uw.cs.lil.events.exp.job.learn;

import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.learner.ILearner;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class DiscreteBeliefLearningJob extends
		AbstractLearningJob<EventsToken, Integer, IMutableSparseVector> {
	public static final Logger											log	= Logger
			.getLogger(DiscreteBeliefLearningJob.class);
	private final EventsDataset											dataset;
	private final ILearner<EventsToken, Integer, IMutableSparseVector>	learner;
	private final IModel<EventsToken, Integer, IMutableSparseVector>	model;

	public DiscreteBeliefLearningJob(String id, Set<String> dependencies,
			EventsDataset dataset,
			ILearner<EventsToken, Integer, IMutableSparseVector> learner,
			IModel<EventsToken, Integer, IMutableSparseVector> model) {
		super(id, dependencies);
		this.dataset = dataset;
		this.learner = learner;
		this.model = model;
	}

	@Override
	protected IDataCollection<EventsToken> getData() {
		return new FlatMappedDataCollection<>(dataset, doc -> doc.stream()
				.flatMap(s -> s.stream()).filter(token -> token.isEvent()));
	}

	@Override
	protected Integer getLabel(EventsToken sample) {
		return (int) Math.round(sample.getFactualityRating().getMean());
	}

	@Override
	protected ILearner<EventsToken, Integer, IMutableSparseVector> getLearner() {
		return learner;
	}

	@Override
	protected IModel<EventsToken, Integer, IMutableSparseVector> getModel(
			IDataCollection<EventsToken> data) {
		return model;
	}

	public static class Creator
			implements IResourceCreator<DiscreteBeliefLearningJob> {
		@Override
		public DiscreteBeliefLearningJob create(
				HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DiscreteBeliefLearningJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("train")),
					resourceCreatorRepo.create(
							config.configurationAt("learner"), resourceRepo),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.belief.learn.discrete";
		}
	}
}
