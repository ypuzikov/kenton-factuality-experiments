package edu.uw.cs.lil.events.exp.job.eval;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.events.analysis.stats.DiscreteEvaluationStats;
import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.data.dataset.EventsDataset;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.data.collection.FlatMappedDataCollection;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class DetectionEvalJob
		extends AbstractEvalJob<EventsToken, Boolean, IMutableSparseVector> {
	public static final Logger log = Logger.getLogger(DetectionEvalJob.class);

	private final List<IAnalysis<EventsToken, Boolean, IMutableSparseVector>>			analyses;
	private final DiscreteEvaluationStats<EventsToken, Boolean, IMutableSparseVector>	evaluationAnalysis;

	private final IModel<EventsToken, Boolean, IMutableSparseVector>	model;
	private final EventsDataset											testDataset;

	public DetectionEvalJob(String id, Set<String> dependencies,
			EventsDataset testDataset,
			IModel<EventsToken, Boolean, IMutableSparseVector> model) {
		super(id, dependencies);
		this.testDataset = testDataset;
		this.model = model;
		this.evaluationAnalysis = new DiscreteEvaluationStats<>();
		this.analyses = Arrays.asList(evaluationAnalysis);
	}

	@Override
	protected List<IAnalysis<EventsToken, Boolean, IMutableSparseVector>> getAnalyses() {
		return analyses;
	}

	@Override
	protected Boolean getLabel(EventsToken sample) {
		return sample.isEvent();
	}

	@Override
	protected IModel<EventsToken, Boolean, IMutableSparseVector> getModel() {
		return model;
	}

	@Override
	protected IDataCollection<EventsToken> getTestData() {
		return new FlatMappedDataCollection<>(testDataset,
				doc -> doc.stream().flatMap(s -> s.stream()));
	}

	@Override
	protected void postProcess() {
		log.info(evaluationAnalysis);
	}

	@Override
	protected void sampleProcess(EventsToken sample) {
		// Do nothing
	}

	public static class Creator implements IResourceCreator<DetectionEvalJob> {
		@Override
		public DetectionEvalJob create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new DetectionEvalJob(config.getString("id"),
					ConfigUtils.getStringSet("dependency", config),
					resourceRepo.getResource(config.getString("test")),
					resourceRepo.getResource(config.getString("model")));
		}

		@Override
		public String type() {
			return "job.detection.eval";
		}
	}
}
