package edu.uw.cs.lil.events.exp;

import java.io.File;
import java.io.IOException;

import org.apache.commons.configuration.ConfigurationException;

import edu.uw.cs.lil.events.services.EventsServices;
import edu.uw.cs.lil.events.services.WordNetServices;
import edu.uw.cs.lil.exp.ConfiguredExperiment;

public class EventsExperiment extends ConfiguredExperiment {
	public EventsExperiment(File configFile)
			throws ConfigurationException, IOException {
		super(configFile, new EventsResourceRepository());
		EventsServices.setInstance(new EventsServices.Builder().build());
		WordNetServices.setInstance(new WordNetServices.Builder().build());
	}
}