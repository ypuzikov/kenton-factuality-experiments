package edu.uw.cs.lil.events.timeml.reader;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.timeml.EventInstance;
import edu.uw.cs.lil.events.timeml.TemporalInstance;
import edu.uw.cs.lil.events.timeml.TemporalRelationInstance;
import edu.uw.cs.lil.events.timeml.TimexInstance;
import edu.uw.cs.lil.events.timeml.XMLElement;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.NumericalCrowdRating;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class TimeMLReader extends AbstractXMLReader {
	public static Logger				log		= Logger.getLogger(TimeMLReader.class);
	private static final Set<String>	TAGS	= new HashSet<>(Arrays.asList(
														"DOCID", "TEXT",
														"TIMEX3", "EVENT",
														"MAKEINSTANCE",
														"TLINK", "SLINK",
														"SIGNAL"));

	@Override
	protected Set<String> getRelevantTags() {
		return TAGS;
	}

	@Override
	protected void postProcess(EventsDocument doc) {
		for (final XMLElement element : doc.elements) {
			if (element.getTag().equals("TIMEX3")) {
				final Pair<Integer, Integer> beginPair = findToken(
						doc.beginCharToToken, element.getStartChar(), false);
				final Pair<Integer, Integer> endPair = findToken(
						doc.endCharToToken, element.getEndChar(), true);
				if (!beginPair.first().equals(endPair.first())) {
					throw new RuntimeException(
							"Mention crossing sentence boundary");
				}
				doc.getAnnotation().addTimexInstance(
						new TimexInstance(getTimeMLId(element
								.getAttribute("tid")), beginPair.second(),
								endPair.second(), beginPair.first(), element
										.getAttribute("type"), element
										.getAttribute("value")));
			} else if (element.getTag().equals("EVENT")) {
				doc.eidToElement.put(element.getAttribute("eid"), element);
			} else if (element.getTag().equals("MAKEINSTANCE")) {
				final XMLElement eventElement = doc.eidToElement.get(element
						.getAttribute("eventID"));
				final Pair<Integer, Integer> beginPair = findToken(
						doc.beginCharToToken, eventElement.getStartChar(),
						false);
				final Pair<Integer, Integer> endPair = findToken(
						doc.endCharToToken, eventElement.getEndChar(), true);
				if (beginPair == null || endPair == null) {
					throw new RuntimeException("Bad MAKEINSTANCE from "
							+ doc.getId() + "(" + element.getAttribute("eiid")
							+ ")");
				}
				if (!beginPair.first().equals(endPair.first())) {
					throw new RuntimeException(String.format(
							"Mention crossing sentence boundary: [%s], [%s]",
							doc.get(beginPair.first()).toString(),
							doc.get(endPair.first()).toString()));
				}
				doc.getAnnotation().addEventInstance(
						new EventInstance(getTimeMLId(element
								.getAttribute("eiid")), beginPair.second(),
								endPair.second(), beginPair.first(),
								eventElement.getAttribute("class"),
								new NumericalCrowdRating(-1, 1),
								new NumericalCrowdRating(-1, 3), null));
			} else if (element.getTag().equals("TLINK")) {
				final TemporalInstance arg0;
				if (element.getAttribute("eventInstanceID") != null) {
					arg0 = doc.getAnnotation()
							.getEventInstance(
									getTimeMLId(element
											.getAttribute("eventInstanceID")));
				} else {
					arg0 = doc.getAnnotation().getTimexInstance(
							getTimeMLId(element.getAttribute("timeID")));
				}
				final TemporalInstance arg1;
				if (element.getAttribute("relatedToEventInstance") != null) {
					arg1 = doc.getAnnotation().getEventInstance(
							getTimeMLId(element
									.getAttribute("relatedToEventInstance")));
				} else {
					arg1 = doc.getAnnotation().getTimexInstance(
							getTimeMLId(element.getAttribute("relatedToTime")));
				}
				if (arg0 == null) {
					log.error(String.format("Arg 0 from %s is not found",
							element.getAttribute("lid")));
				}
				if (arg1 == null) {
					log.error(String.format("Arg 1 from %s is not found",
							element.getAttribute("lid")));
				}
				doc.getAnnotation().addRelationInstance(
						new TemporalRelationInstance(getTimeMLId(element
								.getAttribute("lid")), element
								.getAttribute("relType"), arg0, arg1));
			} else if (element.getTag().equals("SLINK")) {
				final TemporalInstance arg0 = doc.getAnnotation()
						.getEventInstance(
								getTimeMLId(element
										.getAttribute("eventInstanceID")));
				final TemporalInstance arg1 = doc
						.getAnnotation()
						.getEventInstance(
								getTimeMLId(element
										.getAttribute("subordinatedEventInstance")));
				doc.getAnnotation().addRelationInstance(
						new TemporalRelationInstance(getTimeMLId(element
								.getAttribute("lid")), element
								.getAttribute("relType"), arg0, arg1));
			} else if (element.getTag().equals("ALINK")) {
				final TemporalInstance arg0 = doc.getAnnotation()
						.getEventInstance(
								getTimeMLId(element
										.getAttribute("eventInstanceID")));
				final TemporalInstance arg1 = doc
						.getAnnotation()
						.getEventInstance(
								getTimeMLId(element
										.getAttribute("relatedToEventInstance")));
				doc.getAnnotation().addRelationInstance(
						new TemporalRelationInstance(getTimeMLId(element
								.getAttribute("lid")), element
								.getAttribute("relType"), arg0, arg1));
			}
		}
	}

	@Override
	protected void preProcess(File f) {
		// Nothing to do
	}

	public static class Creator implements IResourceCreator<TimeMLReader> {
		private final String	type;

		public Creator() {
			this("reader.timeml");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public TimeMLReader create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new TimeMLReader();
		}

		@Override
		public String type() {
			return type;
		}
	}
}