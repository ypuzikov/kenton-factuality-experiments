package edu.uw.cs.lil.events.timeml.reader;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.data.EventsDocument;

public interface IEventsReader {
	public static final Logger log = Logger.getLogger(IEventsReader.class);

	EventsDocument doReadDocument(File f);

	default EventsDocument readDocument(File f) {
		EventsDocument document;
		final File serializedFile = new File(f.getPath() + ".ser");
		if (serializedFile.exists()) {
			log.info(String.format("Serialized file %s found",
					serializedFile.getName()));
			try {
				document = EventsDocument.deserialize(serializedFile);
				log.info("Deserialization succeeded");
				return document;
			} catch (ClassNotFoundException | IOException e) {
				log.info("Deserialization failed: " + e.getMessage());
			}
		}
		log.info("Reading " + f.getName());
		document = doReadDocument(f);
		document.serialize(serializedFile);
		return document;
	}
}
