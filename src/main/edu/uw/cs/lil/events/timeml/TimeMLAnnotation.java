package edu.uw.cs.lil.events.timeml;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.data.EventsSentence;

public class TimeMLAnnotation implements Serializable {
	private static final long								serialVersionUID	= 5057110732034689261L;
	private final Map<Integer, EventInstance>				events;
	private int												maxEventId;
	private int												maxRelationId;
	private int												maxTimexId;
	private final Map<Integer, TemporalRelationInstance>	relations;
	private final Map<Integer, List<TemporalInstance>>		sentenceToTemporalInstances;
	private final Map<Integer, TimexInstance>				timexes;

	public TimeMLAnnotation() {
		this.events = new HashMap<>();
		this.timexes = new HashMap<>();
		this.relations = new HashMap<>();
		this.sentenceToTemporalInstances = new HashMap<>();
		this.clear();
	}

	public void addEventInstance(EventInstance e) {
		events.put(e.getId(), e);
		final List<TemporalInstance> sentenceEvents = sentenceToTemporalInstances
				.getOrDefault(e.getSentenceIndex(), new LinkedList<>());
		sentenceEvents.add(e);
		sentenceToTemporalInstances.put(e.getSentenceIndex(), sentenceEvents);
		maxEventId = Math.max(maxEventId, e.getId());
	}

	public void addRelationInstance(TemporalRelationInstance r) {
		relations.put(r.getId(), r);
		maxRelationId = Math.max(maxRelationId, r.getId());
	}

	public void addTimexInstance(TimexInstance t) {
		timexes.put(t.getId(), t);
		final List<TemporalInstance> sentenceEvents = sentenceToTemporalInstances
				.getOrDefault(t.getSentenceIndex(), new LinkedList<>());
		sentenceEvents.add(t);
		sentenceToTemporalInstances.put(t.getSentenceIndex(), sentenceEvents);
		maxTimexId = Math.max(maxTimexId, t.getId());
	}

	public void clear() {
		events.clear();
		relations.clear();
		sentenceToTemporalInstances.clear();
		timexes.clear();
		maxEventId = 0;
		maxRelationId = 0;
		maxTimexId = 0;
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof TimeMLAnnotation) {
			final TimeMLAnnotation otherAnnotation = (TimeMLAnnotation) other;
			return Objects.equals(this.events, otherAnnotation.events)
					&& Objects.equals(this.relations, otherAnnotation.relations)
					&& Objects.equals(this.timexes, otherAnnotation.timexes);
		} else {
			return false;
		}
	}

	public EventInstance getEventInstance(int id) {
		return events.get(id);
	}

	public Collection<EventInstance> getEventInstances() {
		return events.values();
	}

	public int getMaxEventId() {
		return maxEventId;
	}

	public int getMaxRelationId() {
		return maxRelationId;
	}

	public int getMaxTimexId() {
		return maxTimexId;
	}

	public TemporalRelationInstance getRelationInstance(int id) {
		return relations.get(id);
	}

	public Collection<TemporalRelationInstance> getRelationInstances() {
		return relations.values();
	}

	public TemporalInstance getTemporalInstance(int sentenceIndex,
			int tokenIndex) {
		for (final TemporalInstance i : sentenceToTemporalInstances
				.getOrDefault(sentenceIndex, Collections.emptyList())) {
			if (i.getStart() <= tokenIndex && i.getEnd() >= tokenIndex) {
				return i;
			}
		}
		return null;
	}

	public List<TemporalInstance> getTemporalInstances(int sentenceIndex) {
		return sentenceToTemporalInstances.getOrDefault(sentenceIndex,
				Collections.emptyList());
	}

	public TimexInstance getTimexInstance(int id) {
		return timexes.get(id);
	}

	public Collection<TimexInstance> getTimexInstances() {
		return timexes.values();
	}

	@Override
	public int hashCode() {
		return Objects.hash(events, relations, timexes);
	}

	public String toString(EventsDocument document) {
		return Stream
				.concat(Stream.concat(events.values().stream(),
						timexes.values().stream()), relations.values().stream())
				.map(t -> t.toPrettyString(document))
				.collect(Collectors.joining("\n"));
	}

	public String toString(EventsSentence sentence, EventsDocument document) {
		return Stream
				.concat(Stream.concat(events.values().stream(),
						timexes.values().stream()), relations.values().stream())
				.filter(instance -> instance.isRelevant(sentence))
				.map(t -> t.toPrettyString(document))
				.collect(Collectors.joining("\n"));
	}
}
