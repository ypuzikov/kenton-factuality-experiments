package edu.uw.cs.lil.events.data;

import java.io.Serializable;

import edu.uw.cs.lil.events.timeml.TimeMLAnnotation;
import edu.uw.cs.lil.events.timeml.TimexInstance;

public class EventsDocumentState implements Serializable {
	private static final long serialVersionUID = 5602095004640625127L;
	private TimeMLAnnotation	annotation;
	private TimexInstance		dct;
	private String				id;

	public EventsDocumentState() {
		this.annotation = new TimeMLAnnotation();
	}

	public EventsDocumentState(String id, TimexInstance dct,
			TimeMLAnnotation annotation) {
		this.id = id;
		this.dct = dct;
		this.annotation = annotation;
	}

	public TimeMLAnnotation getAnnotation() {
		return annotation;
	}

	public TimexInstance getDCT() {
		return dct;
	}

	public String getId() {
		return id;
	}

	public void setAnnotation(TimeMLAnnotation annotation) {
		this.annotation = annotation;
	}

	public void setDCT(TimexInstance dct) {
		this.dct = dct;
	}

	public void setId(String id) {
		this.id = id;
	}
}