package edu.uw.cs.lil.events.data.dataset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import edu.uw.cs.lil.events.data.EventsDocument;
import edu.uw.cs.lil.events.data.EventsSentence;
import edu.uw.cs.lil.events.timeml.EventInstance;
import edu.uw.cs.lil.events.timeml.TimeMLAnnotation;
import edu.uw.cs.lil.events.timeml.reader.IEventsReader;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.ConfigUtils;
import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.NumericalCrowdRating;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class EventsDataset implements IDataCollection<EventsDocument>,
		Serializable {
	private static final long					serialVersionUID	= -7961432091414299838L;

	public static final Logger					log					= Logger.getLogger(EventsDataset.class);

	private final Map<String, EventsDocument>	documents;

	EventsDataset(Map<String, EventsDocument> documents) {
		this.documents = documents;
	}

	public static EventsDataset compose(List<EventsDataset> datasets) {
		return new EventsDataset(datasets
				.stream()
				.flatMap(d -> d.documents.entrySet().stream())
				.collect(
						Collectors
								.toMap(Map.Entry::getKey, Map.Entry::getValue)));
	}

	public static EventsDataset deserialize(File inFile) throws IOException,
			ClassNotFoundException {
		try (final ObjectInputStream in = new ObjectInputStream(
				new FileInputStream(inFile))) {
			return (EventsDataset) in.readObject();
		}
	}

	public static EventsDataset fromBeliefAnnotations(
			List<EventsDocument> documents, File beliefAnnotationsFile) {
		final Map<String, TimeMLAnnotation> annotations = DataUtils
				.mapToMap(
						getBeliefAnnotationMap(beliefAnnotationsFile),
						ratings -> {
							final TimeMLAnnotation a = new TimeMLAnnotation();
							ratings.forEach((sentence, sentenceRatings) -> sentenceRatings.forEach((
									token, rating) -> a
									.addEventInstance(new EventInstance(a
											.getMaxEventId() + 1, token, token,
											sentence, "OCCURRENCE",
											new NumericalCrowdRating(-1, 1),
											rating, null))));
							return a;
						});
		return new EventsDataset(documents.stream()
				.filter(d -> annotations.containsKey(d.getId()))
				.map(d -> d.withAnnotation(annotations.get(d.getId())))
				.collect(Collectors.toMap(d -> d.getId(), d -> d)));
	}

	public static EventsDataset fromReader(List<String> dirs,
			IEventsReader reader) {
		return new EventsDataset(dirs
				.stream()
				.flatMap(dir -> {
					try {
						return Files.walk(Paths.get(dir));
					} catch (final IOException e) {
						throw new RuntimeException(e);
					}
				})
				.filter(filePath -> Files.isRegularFile(filePath)
						&& (filePath.toString().endsWith("ml") || filePath
								.toString().endsWith("csv")))
				.map(filePath -> reader.readDocument(filePath.toFile()))
				.collect(Collectors.toMap(d -> d.getId(), d -> d)));
	}

	private static Map<String, TimeMLAnnotation> getAnnotations(
			File eventsAnnotationFile, File beliefAnnotationsFile,
			EventsDataset referent) {
		final Map<String, Map<Integer, Map<Integer, NumericalCrowdRating>>> beliefAnnotation = getBeliefAnnotationMap(beliefAnnotationsFile);
		final Map<String, TimeMLAnnotation> annotations = new HashMap<>();
		try {
			Files.lines(eventsAnnotationFile.toPath())
					.forEach(
							line -> {
								try {
									final JSONObject root = new JSONObject(line);
									if (root.getString("state")
											.equals("golden")) {
										return;
									}
									final JSONObject data = root
											.getJSONObject("data");
									final String docId = data
											.getString("document");
									final int eventIndex = getInt(data
											.getString("event"));
									final int sentenceIndex = getInt(data
											.getString("sentence"));
									final List<String> tokens = DataUtils
											.jsonToList(data
													.getJSONArray("tokens"));
									final EventsDocument document = referent
											.stream()
											.filter(d -> d.getId()
													.equals(docId)).findFirst()
											.get();
									final EventsSentence sentence = document
											.get(sentenceIndex);
									if (sentence == null) {
										throw new IllegalArgumentException(
												"Annotation out of sync with referent document. Unknown sentence at "
														+ docId + "_"
														+ sentenceIndex);
									} else {
										final List<String> referenceTokens = sentence
												.stream().map(t -> t.getText())
												.collect(Collectors.toList());
										if (!tokens.equals(referenceTokens)) {
											log.error("Annotation out of sync with referent document. Different sentences at "
													+ docId
													+ "_"
													+ sentenceIndex
													+ "ref="
													+ referenceTokens
													+ ", annotation=" + tokens);
										}
									}
									final JSONArray judgments = root
											.getJSONObject("results")
											.getJSONArray("judgments");
									final List<Pair<Integer, ? extends Number>> eventJudgments = new ArrayList<>(
											judgments.length());
									for (int i = 0; i < judgments.length(); i++) {
										final JSONObject judgment = judgments
												.getJSONObject(i);
										eventJudgments.add(Pair.of(
												judgment.getInt("worker_id"),
												judgment.getJSONObject("data")
														.getInt("event_question")));
									}
									final NumericalCrowdRating eventRating = new NumericalCrowdRating(
											eventJudgments);

									final TimeMLAnnotation a = annotations
											.computeIfAbsent(
													docId,
													id -> new TimeMLAnnotation());
									a.addEventInstance(new EventInstance(
											a.getMaxEventId() + 1,
											eventIndex,
											eventIndex,
											sentenceIndex,
											"OCCURRENCE",
											eventRating,
											Optional.ofNullable(
													beliefAnnotation.get(docId))
													.map(sentenceMap -> sentenceMap
															.get(sentenceIndex))
													.map(eventMap -> eventMap
															.get(eventIndex))
													.orElseGet(
															() -> new NumericalCrowdRating(
																	-1, 0)),
											null));
								} catch (final JSONException e) {
									throw new RuntimeException(e);
								}
							});
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		return annotations;
	}

	private static Map<String, Map<Integer, Map<Integer, NumericalCrowdRating>>> getBeliefAnnotationMap(
			File annotationFile) {
		final Map<String, Map<Integer, Map<Integer, NumericalCrowdRating>>> beliefAnnotations = new HashMap<>();
		try {
			Files.lines(annotationFile.toPath())
					.forEach(
							line -> {
								try {
									final JSONObject root = new JSONObject(line);
									if (root.getString("state")
											.equals("golden")) {
										return;
									}
									final JSONObject data = root
											.getJSONObject("data");
									final String documentId = data
											.getString("document");
									final int eventIndex = getInt(data
											.getString("event"));
									final int sentenceIndex = getInt(data
											.getString("sentence"));
									final JSONArray judgments = root
											.getJSONObject("results")
											.getJSONArray("judgments");

									final List<Pair<Integer, ? extends Number>> beliefJudgments = new ArrayList<>(
											judgments.length());
									for (int i = 0; i < judgments.length(); i++) {
										final JSONObject judgment = judgments
												.getJSONObject(i);
										beliefJudgments.add(Pair.of(
												judgment.getInt("worker_id"),
												judgment.getJSONObject("data")
														.getInt("belief_question")));
									}
									final NumericalCrowdRating beliefRating = new NumericalCrowdRating(
											beliefJudgments);

									beliefAnnotations
											.computeIfAbsent(documentId,
													id -> new HashMap<>())
											.computeIfAbsent(sentenceIndex,
													i -> new HashMap<>())
											.put(eventIndex, beliefRating);
								} catch (final JSONException e) {
									throw new RuntimeException(e);
								}
							});
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
		return beliefAnnotations;
	}

	private static int getInt(Object x) {
		return x instanceof String ? Integer.parseInt((String) x) : ((Long) x)
				.intValue();
	}

	public EventsDataset fromSubset(EventsDataset subset) {
		final Set<String> subsetIds = subset.stream()
				.map(EventsDocument::getId).collect(Collectors.toSet());
		return new EventsDataset(this.stream()
				.filter(doc -> subsetIds.contains(doc.getId()))
				.collect(Collectors.toMap(EventsDocument::getId, x -> x)));
	}

	public EventsDocument get(String docId) {
		return documents.get(docId);
	}

	@Override
	public Iterator<EventsDocument> iterator() {
		return documents.values().iterator();
	}

	public void serialize(File outFile) {
		try (final ObjectOutputStream out = new ObjectOutputStream(
				new FileOutputStream(outFile))) {
			out.writeObject(this);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public int size() {
		return documents.size();
	}

	@Override
	public Stream<EventsDocument> stream() {
		return documents.values().stream();
	}

	public void toTimeML(TimeMLAnnotation currentAnnotation, File outputDir) {
		for (final EventsDocument d : this) {
			d.toTimeML(currentAnnotation, new File(outputDir, d.getId()
					+ ".tml"));
		}
	}

	private EventsDataset withAnnotations(
			List<HierarchicalConfiguration> annotationConfigs) {
		final Map<String, TimeMLAnnotation> annotations = annotationConfigs
				.stream()
				.map(config -> getAnnotations(
						ConfigUtils.getFile("events", config),
						ConfigUtils.getFile("belief", config), this))
				.peek(a -> log.info(a.size() + " documents in annotation"))
				.collect(
						DataUtils.mergeCollector((x, y) -> {
							throw new IllegalArgumentException(
									"Redundant annotations");
						}));
		return new EventsDataset(this.stream()
				.filter(d -> annotations.containsKey(d.getId()))
				.map(d -> d.withAnnotation(annotations.get(d.getId())))
				.collect(Collectors.toMap(d -> d.getId(), d -> d)));
	}

	public static class AnnotationCreator implements
			IResourceCreator<EventsDataset> {

		private final String	type;

		public AnnotationCreator() {
			this("data.events.annotated");
		}

		public AnnotationCreator(String type) {
			this.type = type;
		}

		@Override
		public EventsDataset create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return resourceRepo.<EventsDataset> getResource(
					config.getString("base")).withAnnotations(
					config.configurationsAt("annotation"));
		}

		@Override
		public String type() {
			return type;
		}
	}

	public static class CompositeCreator implements
			IResourceCreator<EventsDataset> {

		private final String	type;

		public CompositeCreator() {
			this("data.events.composite");
		}

		public CompositeCreator(String type) {
			this.type = type;
		}

		@Override
		public EventsDataset create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return EventsDataset.compose(Arrays
					.stream(config.getStringArray("dataset"))
					.map(resourceRepo::<EventsDataset> getResource)
					.collect(Collectors.toList()));
		}

		@Override
		public String type() {
			return type;
		}
	}

	public static class Creator implements IResourceCreator<EventsDataset> {

		private final String	type;

		public Creator() {
			this("data.events");
		}

		public Creator(String type) {
			this.type = type;
		}

		@Override
		public EventsDataset create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return EventsDataset.fromReader(
					Arrays.stream(config.getStringArray("dir")).collect(
							Collectors.toList()),
					resourceRepo.getResource(config.getString("reader")));
		}

		@Override
		public String type() {
			return type;
		}
	}

	public static class SubsetCreator implements
			IResourceCreator<EventsDataset> {

		private final String	type;

		public SubsetCreator() {
			this("data.events.subset");
		}

		public SubsetCreator(String type) {
			this.type = type;
		}

		@Override
		public EventsDataset create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return resourceRepo.<EventsDataset> getResource(
					config.getString("base")).fromSubset(
					resourceRepo.getResource(config.getString("subset")));
		}

		@Override
		public String type() {
			return type;
		}
	}
}
