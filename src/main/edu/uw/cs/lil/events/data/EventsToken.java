package edu.uw.cs.lil.events.data;

import java.util.stream.Stream;

import edu.uw.cs.lil.events.timeml.EventInstance;
import edu.uw.cs.lil.events.timeml.TemporalInstance;
import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.NumericalCrowdRating;
import edu.uw.cs.lil.utils.data.text.ISituatedToken;
import edu.uw.cs.lil.utils.data.text.IToken;

public class EventsToken implements ISituatedToken<EventsSentence> {
	private static final long serialVersionUID = 3134003119648660534L;
	private final EventsSentence	state;
	private final IToken			token;

	public EventsToken(IToken token, EventsSentence state) {
		this.token = token;
		this.state = state;
	}

	public Stream<EventsToken> ancestorStream() {
		if (this.getGovernor() < 0) {
			return Stream.empty();
		} else {
			return DataUtils.append(state.get(this.getGovernor())
					.ancestorStream(), state.get(this.getGovernor()));
		}
	}

	public Stream<EventsToken> childStream() {
		return state.stream().filter(
				child -> child.getGovernor() == this.getIndex());
	}

	@Override
	public String getAfter() {
		return token.getAfter();
	}

	@Override
	public String getBefore() {
		return token.getBefore();
	}

	@Override
	public int getCharEnd() {
		return token.getCharEnd();
	}

	@Override
	public int getCharStart() {
		return token.getCharStart();
	}

	@Override
	public String getDependencyType() {
		return token.getDependencyType();
	}

	public NumericalCrowdRating getEventDistribution() {
		final TemporalInstance instance = state.getTemporalInstance(getIndex());
		if (!(instance instanceof EventInstance)) {
			return new NumericalCrowdRating(-1, 0);
		} else {
			return ((EventInstance) instance).getEventRating();
		}
	}

	public String getFactbankRating() {
		final TemporalInstance instance = state.getTemporalInstance(getIndex());
		if (!(instance instanceof EventInstance)) {
			throw new RuntimeException(
					"Should not be asking for factuality for non-events.");
		} else {
			return ((EventInstance) instance).getFactbankRating();
		}
	}

	public NumericalCrowdRating getFactualityRating() {
		final TemporalInstance instance = state.getTemporalInstance(getIndex());
		if (!(instance instanceof EventInstance)) {
			throw new RuntimeException(
					"Should not be asking for factuality for non-events.");
		} else {
			return ((EventInstance) instance).getFactualityRating();
		}
	}

	@Override
	public int getGovernor() {
		return token.getGovernor();
	}

	@Override
	public int getIndex() {
		return token.getIndex();
	}

	@Override
	public EventsSentence getState() {
		return state;
	}

	@Override
	public String getTag() {
		return token.getTag();
	}

	@Override
	public String getText() {
		return token.getText();
	}

	public boolean isEvent() {
		return getEventDistribution().getMean() > 0.5;
	}

	public boolean isEvent(int maxSamples) {
		return getEventDistribution().getMean(maxSamples) > 0.5;
	}

	@Override
	public String toString() {
		return getText();
	}
}
