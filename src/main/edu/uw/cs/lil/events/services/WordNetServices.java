package edu.uw.cs.lil.events.services;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.yawni.wordnet.POS;
import org.yawni.wordnet.RelationType;
import org.yawni.wordnet.WordNet;
import org.yawni.wordnet.WordSense;

import edu.uw.cs.lil.utils.DataUtils;

/**
 * Various events services that are used throughout the system. This service
 * class must be initialized after {@link WordNetServices}.
 *
 * @author Kenton Lee
 */
public class WordNetServices {
	private static WordNetServices			INSTANCE;

	private final Map<WordSense, Boolean>	eventHyponymCache;
	private final Map<String, String>		lemmaCache;
	private final Map<String, POS>			tagMap;
	private final WordNet					wn;

	private WordNetServices() {
		wn = WordNet.getInstance();
		lemmaCache = new ConcurrentHashMap<>();
		eventHyponymCache = new ConcurrentHashMap<>();
		tagMap = new ConcurrentHashMap<>();
		tagMap.put("VB", POS.VERB);
		tagMap.put("NN", POS.NOUN);
		tagMap.put("JJ", POS.ADJ);
		tagMap.put("RB", POS.ADV);
	}

	public static List<WordSense> getDerivationallyRelatedWords(String w,
			String tag) {
		return INSTANCE.wn
				.lookupWordSenses(w, getWordNetPOS(tag))
				.stream()
				.flatMap(
						ws -> ws.getRelationTargets(
								RelationType.DERIVATIONALLY_RELATED).stream())
				.flatMap(args -> DataUtils.stream(args))
				.collect(Collectors.toList());
	}

	public static String getLemma(String word) {
		return getLemma(word, null);
	}

	public static String getLemma(String word, String tag) {
		final String lowerWord = word.toLowerCase();
		if (!INSTANCE.lemmaCache.containsKey(lowerWord)) {
			INSTANCE.lemmaCache.put(
					lowerWord,
					INSTANCE.wn
							.lookupBaseForms(word.toLowerCase(),
									getWordNetPOS(tag)).stream().findFirst()
							.orElse(lowerWord));
		}
		return INSTANCE.lemmaCache.get(lowerWord).toLowerCase();
	}

	public static Set<String> getSynonyms(String w) {
		return getSynonyms(w, null);
	}

	public static Set<String> getSynonyms(String w, String tag) {
		return INSTANCE.wn.lookupWordSenses(w, getWordNetPOS(tag)).stream()
				.map(ws -> ws.getSynset())
				.flatMap(ss -> ss.getWordSenses().stream())
				.map(ws -> ws.getLemma()).distinct()
				.collect(Collectors.toSet());
	}

	public static POS getWordNetPOS(String tag) {
		return tag == null || tag.length() < 2 ? POS.ALL : INSTANCE.tagMap
				.getOrDefault(tag.toUpperCase().substring(0, 2), POS.ALL);
	}

	public static boolean isEventHyponym(String word, String tag) {
		return INSTANCE.wn
				.lookupSynsets(word.toLowerCase(), getWordNetPOS(tag)).stream()
				.flatMap(ss -> ss.getWordSenses().stream())
				.anyMatch(ws -> isEventHyponym(ws, new HashSet<>()));
	}

	public static boolean isVerbDerivative(String w) {
		return isVerbDerivative(w, null);
	}

	public static boolean isVerbDerivative(String w, String tag) {
		return Stream.concat(
				getDerivationallyRelatedWords(w, tag).stream(),
				INSTANCE.wn
						.lookupBaseForms(w, getWordNetPOS(tag))
						.stream()
						.flatMap(
								l -> INSTANCE.wn.lookupWordSenses(l,
										getWordNetPOS(tag)).stream()))
				.anyMatch(ws -> ws.getPOS() == POS.VERB);
	}

	public static void setInstance(WordNetServices instance) {
		INSTANCE = instance;
	}

	private static boolean isEventHyponym(WordSense ws, Set<WordSense> explored) {
		if (explored.contains(ws)) {
			return false;
		} else if (INSTANCE.eventHyponymCache.containsKey(ws)) {
			return INSTANCE.eventHyponymCache.get(ws);
		} else if (ws.getLemma().equals("event")) {
			return true;
		} else {
			explored.add(ws);
			INSTANCE.eventHyponymCache
					.put(ws,
							ws.getRelationTargets(RelationType.HYPERNYM)
									.stream()
									.flatMap(args -> DataUtils.stream(args))
									.anyMatch(
											parent -> isEventHyponym(parent,
													explored)));
			return INSTANCE.eventHyponymCache.get(ws);
		}
	}

	public static class Builder {
		public WordNetServices build() {
			return new WordNetServices();
		}
	}
}