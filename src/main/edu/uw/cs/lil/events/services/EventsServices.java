package edu.uw.cs.lil.events.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;
import edu.uw.cs.lil.events.data.Token;
import edu.uw.cs.lil.events.external.distributional.BrownClusters;
import edu.uw.cs.lil.utils.data.text.IToken;

/**
 * Various events services that are used throughout the system.
 *
 * @author Kenton Lee
 */
public class EventsServices {
	public static final Logger					log	= Logger
			.getLogger(EventsServices.class);
	private static EventsServices				INSTANCE;

	private final Map<String, BrownClusters>	brownClustersDictionaries;
	private GrammaticalStructureFactory			gsf;
	private LexicalizedParser					parser;
	private StanfordCoreNLP						stanfordPipeline;

	private EventsServices() {
		brownClustersDictionaries = new HashMap<>();
		loadBrownClusters(new File("resources/brown-clusters/c100.txt"));
		loadBrownClusters(new File("resources/brown-clusters/c320.txt"));
		loadBrownClusters(new File("resources/brown-clusters/c1000.txt"));
		loadBrownClusters(new File("resources/brown-clusters/c3200.txt"));
	}

	public synchronized static List<IToken> dependencyParse(
			List<IToken> tokens) {
		if (tokens.isEmpty()) {
			return Collections.emptyList();
		} else {
			return getTokens(tokens, INSTANCE.getGsf()
					.newGrammaticalStructure(INSTANCE.getParser()
							.apply(tokens.stream().map(token -> new HasWord() {
								private static final long serialVersionUID = -1004748002421978268L;

								@Override
								public void setWord(String word) {
									throw new UnsupportedOperationException();
								}

								@Override
								public String word() {
									return token.getText();
								}
							}).collect(Collectors.toList())))
					.typedDependencies());
		}
	}

	public static BrownClusters getBrownClusters(String name) {
		return INSTANCE.brownClustersDictionaries.get(name);
	}

	public static List<IToken> getTokens(List<CoreLabel> stanfordTokens) {
		for (int i = 0; i < stanfordTokens.size(); i++) {
			stanfordTokens.get(i).setIndex(i);
		}
		final List<IToken> tokens = stanfordTokens.stream()
				.map(t -> new Token(t)).collect(Collectors.toList());
		for (int i = 0; i < tokens.size(); i++) {
			if (tokens.get(i).getIndex() != i) {
				throw new IllegalArgumentException(
						"Tokens out of order: index of "
								+ tokens.get(i).getText() + " is "
								+ tokens.get(i).getIndex());
			}
		}
		return tokens;
	}

	public static List<IToken> getTokens(List<IToken> tokens,
			Collection<TypedDependency> dependencies) {
		final List<TypedDependency> sortedDependencies = dependencies.stream()
				.sorted().collect(Collectors.toList());
		final List<IToken> mergedTokens = new ArrayList<>();
		for (int i = 0; i < tokens.size(); i++) {
			mergedTokens
					.add(new Token(tokens.get(i), sortedDependencies.get(i)));
		}
		return mergedTokens;
	}

	public synchronized static List<List<IToken>> preprocessText(String text) {
		final Annotation a = new Annotation(text);
		INSTANCE.getStanfordPipeline().annotate(a);
		final List<List<IToken>> tokensList = new ArrayList<>();
		for (final CoreMap sentenceAnnotation : a
				.get(SentencesAnnotation.class)) {
			tokensList.add(dependencyParse(
					getTokens(sentenceAnnotation.get(TokensAnnotation.class))));
		}
		return tokensList;
	}

	public static void setInstance(EventsServices instance) {
		EventsServices.INSTANCE = instance;
	}

	private synchronized GrammaticalStructureFactory getGsf() {
		if (gsf == null) {
			gsf = new PennTreebankLanguagePack()
					.grammaticalStructureFactory(x -> true);
		}
		return gsf;
	}

	private synchronized LexicalizedParser getParser() {
		if (parser == null) {
			parser = LexicalizedParser.loadModel();
		}
		return parser;
	}

	private synchronized StanfordCoreNLP getStanfordPipeline() {
		if (stanfordPipeline == null) {
			final Properties props = new Properties();
			props.put("annotators", "tokenize, ssplit, pos");
			stanfordPipeline = new StanfordCoreNLP(props);
		}
		return stanfordPipeline;
	}

	private void loadBrownClusters(File clustersFile) {
		brownClustersDictionaries.put(clustersFile.getName(),
				new BrownClusters(clustersFile));
	}

	public static class Builder {
		public EventsServices build() {
			return new EventsServices();
		}
	}
}