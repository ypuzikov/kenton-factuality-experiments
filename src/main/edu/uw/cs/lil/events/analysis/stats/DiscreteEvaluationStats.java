package edu.uw.cs.lil.events.analysis.stats;

import java.util.EnumSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.FormatUtils;
import edu.uw.cs.lil.utils.MathUtils;
import edu.uw.cs.lil.utils.data.ConcurrentDefaultHashMap;

public class DiscreteEvaluationStats<V, A extends Comparable<A>, P>
		implements IAnalysis<V, A, P> {
	public static Logger							log	= Logger
			.getLogger(DiscreteEvaluationStats.class);
	private final Map<A, Map<A, AtomicInteger>>		confusionMatrix;
	private final Map<A, DiscreteEvaluationCounts>	countsMap;
	private final AtomicInteger						total;

	public DiscreteEvaluationStats() {
		this.countsMap = new ConcurrentDefaultHashMap<>(
				x -> new DiscreteEvaluationCounts());
		this.total = new AtomicInteger();
		this.confusionMatrix = new ConcurrentDefaultHashMap<>(
				x -> new ConcurrentDefaultHashMap<>(y -> new AtomicInteger(0)));
	}

	@Override
	public void analyze(V sample, A predicted, A gold, IModel<V, A, P> model) {
		countsMap.get(predicted).predicted.incrementAndGet();
		countsMap.get(gold).gold.incrementAndGet();
		if (predicted.equals(gold)) {
			countsMap.get(gold).correct.incrementAndGet();
		}
		confusionMatrix.get(gold).get(predicted).incrementAndGet();
		total.incrementAndGet();
	}

	public double getAccuracy() {
		return countsMap.values().stream().mapToInt(c -> c.correct.get()).sum()
				/ total.doubleValue();
	}

	public DiscreteEvaluationCounts getCounts(A a) {
		return countsMap.get(a);
	}

	public double getMacroF1() {
		return countsMap.values().stream()
				.mapToDouble(DiscreteEvaluationCounts::getF1).average()
				.getAsDouble();
	}

	public double getMajorityAccuracy() {
		return countsMap.get(getMajorityClass()).gold.get()
				/ total.doubleValue();
	}

	public A getMajorityClass() {
		return countsMap.entrySet().stream().max((e1, e2) -> Integer
				.compare(e1.getValue().gold.get(), e2.getValue().gold.get()))
				.map(e -> e.getKey()).get();
	}

	public double getMicroF1() {
		return countsMap.values().stream()
				.collect(DiscreteEvaluationCounts.collector()).getF1();
	}

	@Override
	public String toString() {
		return new StringBuffer()
				.append(countsMap.entrySet().stream()
						.map(entry -> entry.getKey() + " stats:\n"
								+ entry.getValue())
				.collect(Collectors.joining("\n")) + "\n")
				.append(String.format("Micro F1: %.3f\n", 100.0 * getMicroF1()))
				.append(String.format("Macro F1: %.3f\n", 100.0 * getMacroF1()))
				.append("Confusion matrix: \n")
				.append(FormatUtils.plotConfusionMatrix(confusionMatrix))
				.append(String.format("Accuracy: %.3f%%\n",
						100.0 * getAccuracy()))
				.append(String.format("Majority accuracy (%s): %.3f%%\n",
						getMajorityClass().toString(),
						100.0 * getMajorityAccuracy()))
				.toString();
	}

	public static class DiscreteEvaluationCounts {
		public final AtomicInteger correct, predicted, gold;

		public DiscreteEvaluationCounts() {
			correct = new AtomicInteger();
			predicted = new AtomicInteger();
			gold = new AtomicInteger();
		}

		public static Collector<DiscreteEvaluationCounts, ?, DiscreteEvaluationCounts> collector() {
			return new Collector<DiscreteEvaluationCounts, DiscreteEvaluationCounts, DiscreteEvaluationCounts>() {
				@Override
				public BiConsumer<DiscreteEvaluationCounts, DiscreteEvaluationCounts> accumulator() {
					return DiscreteEvaluationCounts::merge;
				}

				@Override
				public Set<Characteristics> characteristics() {
					return EnumSet.of(Characteristics.CONCURRENT,
							Characteristics.UNORDERED,
							Characteristics.IDENTITY_FINISH);
				}

				@Override
				public BinaryOperator<DiscreteEvaluationCounts> combiner() {
					return DiscreteEvaluationCounts::merge;
				}

				@Override
				public Function<DiscreteEvaluationCounts, DiscreteEvaluationCounts> finisher() {
					return x -> x;
				}

				@Override
				public Supplier<DiscreteEvaluationCounts> supplier() {
					return DiscreteEvaluationCounts::new;
				}
			};
		}

		public double getF1() {
			if (correct.get() == 0) {
				return 0;
			} else {
				return MathUtils.harmonicMean(getPrecision(), getRecall());
			}
		}

		public double getPrecision() {
			return correct.get() / predicted.doubleValue();
		}

		public double getRecall() {
			return correct.get() / gold.doubleValue();
		}

		public DiscreteEvaluationCounts merge(DiscreteEvaluationCounts other) {
			this.correct.addAndGet(other.correct.get());
			this.predicted.addAndGet(other.predicted.get());
			this.gold.addAndGet(other.gold.get());
			return this;
		}

		@Override
		public String toString() {
			final StringBuffer sb = new StringBuffer();
			sb.append(String.format("Recall: %.3f%% (%d/%d)\n",
					100.0 * getRecall(), correct.get(), gold.get()));
			sb.append(String.format("Precision: %.3f%% (%d/%d)\n",
					100.0 * getPrecision(), correct.get(), predicted.get()));
			sb.append(String.format("F1: %.3f%%", 100.0 * getF1()));
			return sb.toString();
		}
	}
}