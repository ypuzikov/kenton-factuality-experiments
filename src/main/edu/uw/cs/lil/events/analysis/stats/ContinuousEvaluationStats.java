package edu.uw.cs.lil.events.analysis.stats;

import java.util.Collection;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.DoubleStream;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.log4j.Logger;

import com.google.common.util.concurrent.AtomicDouble;

import edu.uw.cs.lil.events.analysis.IAnalysis;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class ContinuousEvaluationStats<V, P>
		implements IAnalysis<V, Double, P> {
	public final static Logger						log	= Logger
			.getLogger(ContinuousEvaluationStats.class);
	private final AtomicDouble						absError;
	private final Collection<Pair<Double, Double>>	predictedAndGold;
	private final AtomicInteger						total;

	public ContinuousEvaluationStats() {
		this.absError = new AtomicDouble();
		this.total = new AtomicInteger();
		this.predictedAndGold = new LinkedBlockingDeque<>();
	}

	public static double denormalize(double x, double min, double max) {
		return (max - min) * x + min;
	}

	public static double normalize(double x, double min, double max) {
		return (x - min) / (max - min);
	}

	@Override
	public void analyze(V sample, Double predicted, Double gold,
			IModel<V, Double, P> model) {
		absError.addAndGet(Math.abs(predicted - gold));
		total.incrementAndGet();
		predictedAndGold.add(Pair.of(predicted, gold));
	}

	public double getCorrelation() {
		return getCorrelation(getPearsonsAnalysis());
	}

	public double getCorrelation(PearsonsCorrelation analysis) {
		return analysis.getCorrelationMatrix().getEntry(0, 1);
	}

	public double getMAE() {
		return absError.get() / total.get();
	}

	public PearsonsCorrelation getPearsonsAnalysis() {
		return new PearsonsCorrelation(
				predictedAndGold
						.stream().map(p -> DoubleStream
								.of(p.first(), p.second()).toArray())
				.toArray(x -> new double[x][]));
	}

	public double getPValue(PearsonsCorrelation analysis) {
		return analysis.getCorrelationPValues().getEntry(0, 1);
	}

	@Override
	public String toString() {
		final StringBuffer sb = new StringBuffer();
		sb.append(String.format("Mean absolute error: %.3f\n", getMAE()));
		final PearsonsCorrelation analysis = getPearsonsAnalysis();
		sb.append("Pearson correlation: " + getCorrelation() + "\n");
		sb.append("Pearson correlation p-value: " + getPValue(analysis));
		return sb.toString();
	}
}