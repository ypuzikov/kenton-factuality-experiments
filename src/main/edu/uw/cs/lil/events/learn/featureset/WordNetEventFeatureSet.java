package edu.uw.cs.lil.events.learn.featureset;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.services.WordNetServices;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class WordNetEventFeatureSet implements IFeatureSet<EventsToken> {
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return SparseVector.of(ObjectKey.of(
				WordNetEventFeatureSet.class,
				WordNetServices.isEventHyponym(dataItem.getText(),
						dataItem.getTag())));
	}
}
