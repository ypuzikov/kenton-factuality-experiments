package edu.uw.cs.lil.events.learn.featureset.columbia;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class VerbTypeFeatureSet implements IFeatureSet<EventsToken> {
	public static String getVerbType(EventsToken dataItem) {
		if (dataItem.getTag().equals("MD")) {
			return "modal";
		} else if (dataItem.getDependencyType().equals("aux")) {
			return "aux";
		} else if (dataItem.getTag().startsWith("VB")) {
			return "reg";
		} else {
			return "nul";
		}
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return SparseVector.of(CompositeKey.of(
				ObjectKey.of(VerbTypeFeatureSet.class),
				ObjectKey.of(getVerbType(dataItem))));
	}
}
