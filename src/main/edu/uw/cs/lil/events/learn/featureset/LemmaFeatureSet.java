package edu.uw.cs.lil.events.learn.featureset;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.events.lemma.ILemmatizer;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.CompositeKey;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class LemmaFeatureSet implements IFeatureSet<EventsToken> {
	private final ILemmatizer	lemmatizer;

	public LemmaFeatureSet(ILemmatizer lemmatizer) {
		this.lemmatizer = lemmatizer;
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return lemmatizer
				.lemmatize(dataItem.getText(), dataItem.getTag())
				.stream()
				.map(lemma -> SparseVector.of(CompositeKey.of(ObjectKey.of(
						LemmaFeatureSet.class, lemma))))
				.collect(SparseVector.sumCollector());
	}
}
