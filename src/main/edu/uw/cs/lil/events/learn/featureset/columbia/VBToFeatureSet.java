package edu.uw.cs.lil.events.learn.featureset.columbia;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class VBToFeatureSet implements IFeatureSet<EventsToken> {
	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		if (dataItem.getTag().equals("VB")
				&& dataItem.childStream().anyMatch(
						child -> child.getText().toLowerCase().equals("to"))) {
			return SparseVector.of(ObjectKey.of(VBToFeatureSet.class));
		} else {
			return SparseVector.EMPTY;
		}
	}
}
