package edu.uw.cs.lil.events.learn.featureset;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import edu.uw.cs.lil.events.data.EventsToken;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public class NGramFeatureSet implements IFeatureSet<EventsToken> {
	private final int							beginOffset, endOffset;
	private final Function<EventsToken, String>	lexicalizer;

	public NGramFeatureSet(int beginOffset, int endOffset,
			Function<EventsToken, String> lexicalizer) {
		this.beginOffset = beginOffset;
		this.endOffset = endOffset;
		this.lexicalizer = lexicalizer;
	}

	@Override
	public ISparseVector getFeatures(EventsToken dataItem) {
		return SparseVector.of(Key.of(
				beginOffset,
				endOffset,
				IntStream.rangeClosed(beginOffset, endOffset)
						.map(i -> i + dataItem.getIndex())
						.mapToObj(i -> indexToEntry(dataItem, i))));
	}

	private Entry indexToEntry(EventsToken token, int i) {
		if (i < 0) {
			return Entry.of(Entry.Type.START);
		} else if (i >= token.getState().size()) {
			return Entry.of(Entry.Type.END);
		} else {
			return Entry.of(Entry.Type.LEXICAL,
					lexicalizer.apply(token.getState().get(i)));
		}
	}

	public static class Key implements ISparseKey {
		private static final long serialVersionUID = 7273105219921969104L;
		private final int						start;
		private final int						end;
		private final ImmutableList<Entry>		ngram;
		private final int						hashCode;
		private final Pair<Integer, Integer>	structure;
		private final ImmutableList<String>		lexicalItems;

		private Key(int start, int end, Stream<Entry> ngram) {
			this.start = start;
			this.end = end;
			this.ngram = ngram.collect(DataUtils.immutableListCollector());
			this.hashCode = Objects.hash(ngram);
			this.structure = Pair.of(start, end);
			this.lexicalItems = this.ngram.stream()
					.filter(entry -> entry.type == Entry.Type.LEXICAL)
					.map(entry -> entry.target)
					.collect(DataUtils.immutableListCollector());
		}

		private static Key of(int start, int end, Stream<Entry> ngram) {
			return new Key(start, end, ngram);
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (other instanceof Key) {
				return Objects.equals(this.start, ((Key) other).start)
						&& Objects.equals(this.end, ((Key) other).end)
						&& Objects.equals(this.ngram, ((Key) other).ngram);
			} else {
				return false;
			}
		}

		@Override
		public Object getStructure() {
			return structure;
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		@Override
		public int lexicalSize() {
			return lexicalItems.size();
		}

		@Override
		public Stream<String> lexicalStream() {
			return lexicalItems.stream();
		}

		@Override
		public String toString() {
			return String.format(
					"[%d:%d]:%s",
					start,
					end,
					ngram.stream().map(Object::toString)
							.collect(Collectors.joining("+")));
		}
	}

	private static class Entry implements Serializable {
		private static final long serialVersionUID = 7480838101330877479L;
		public Type			type;
		public String		target;
		private final int	hashCode;

		private Entry(Type type, String target) {
			if (type == Type.LEXICAL && target == null) {
				throw new IllegalArgumentException(
						"Lexical types should have non-null target");
			}
			this.type = type;
			this.target = target;
			this.hashCode = Objects.hash(type, target);
		}

		public static Entry of(Type type) {
			return new Entry(type, null);
		}

		public static Entry of(Type type, String target) {
			return new Entry(type, target);
		}

		@Override
		public boolean equals(Object other) {
			if (other == null) {
				return false;
			}
			if (other instanceof Key) {
				return Objects.equals(this.type, ((Entry) other).type)
						&& Objects.equals(this.target, ((Entry) other).target);
			} else {
				return false;
			}
		}

		@Override
		public int hashCode() {
			return hashCode;
		}

		@Override
		public String toString() {
			return type == Type.LEXICAL ? target : type.toString();
		}

		public enum Type {
			LEXICAL, START, END
		}
	}
}
