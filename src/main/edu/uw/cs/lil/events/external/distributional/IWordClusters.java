package edu.uw.cs.lil.events.external.distributional;


public interface IWordClusters {
	String getCluster(String word);

	String getClusteringType();
}