package edu.uw.cs.lil.events.external.svm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.utils.data.tuple.Pair;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.key.ISparseKey;

public class SVMClassification {
	private static final Logger	log	= Logger.getLogger(SVMClassification.class);

	private SVMClassification() {
	}

	public static ISparseVector learnModel(List<Sample> samples) {
		log.debug(String.format("Beginning training over %d samples",
				samples.size()));

		final ISparseKey[] featureMap = SVMLightUtils.getFeatureMap(samples);
		final Map<ISparseKey, Integer> inverseFeatureMap = SVMLightUtils
				.getInverseFeatureMap(featureMap);

		log.debug(String.format("Created feature map of size %d",
				featureMap.length));

		try {
			final File trainingFile = File.createTempFile("temp", ".dat");
			try (final BufferedWriter writer = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(trainingFile)))) {
				for (final Sample sample : samples) {
					writer.write(sample.toString(inverseFeatureMap));
				}
			}
			log.debug("Training file: " + trainingFile.getAbsolutePath());

			final File modelFile = File.createTempFile("temp", ".model");

			final ProcessBuilder processBuilder = new ProcessBuilder(
					"resources/svm_rank/svm_light/svm_learn", "-b", "0", "-z",
					"c", "-c", "1.0", trainingFile.getAbsolutePath(),
					modelFile.getAbsolutePath()).redirectErrorStream(true);

			final Process process = processBuilder.start();
			final BufferedReader br = new BufferedReader(new InputStreamReader(
					process.getInputStream()));
			String line;
			log.debug("Running: "
					+ processBuilder.command().stream()
							.collect(Collectors.joining(" ")));
			while ((line = br.readLine()) != null) {
				log.debug(line);
			}
			final int exitValue = process.waitFor();
			log.debug("Exit Value is " + exitValue);

			trainingFile.delete();

			log.debug("Model file: " + modelFile.getAbsolutePath());
			final ISparseVector weights = SVMLightUtils.readModel(modelFile,
					featureMap);
			modelFile.delete();
			return weights;
		} catch (final IOException | InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public static double testModel(ISparseVector weights, List<Sample> samples,
			String label) {
		log.debug(weights);
		double count = 0;
		double correct = 0;
		for (final Sample s : samples) {
			count++;
			final boolean prediction = s.features.dotProduct(weights) > 0;
			log.debug("prediction: " + prediction + ", gold: " + s.target);
			if (s.target == prediction) {
				correct++;
			}
		}
		log.debug(String.format("%s accuracy: %.3f", label, correct / count));
		return correct / count;
	}

	public static class Sample extends AbstractSVMSample<Boolean> {
		private Sample(boolean target, double cost, ISparseVector features) {
			super(target, cost, features);
		}

		public static Sample of(boolean target, double cost,
				ISparseVector features) {
			return new Sample(target, cost, features);
		}

		@Override
		public String toString(Map<ISparseKey, Integer> inverseFeatureMap) {
			return String.format(
					"%d cost:%f %s\n",
					target ? 1 : -1,
					cost,
					features.stream()
							.map(entry -> Pair.of(
									inverseFeatureMap.get(entry.first()),
									entry.second()))
							.sorted((x, y) -> Integer.compare(x.first(),
									y.first()))
							.map(entry -> entry.first() + 1 + ":"
									+ entry.second())
							.collect(Collectors.joining(" ")));
		}
	}

}
