package edu.uw.cs.lil.utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import edu.uw.cs.lil.utils.data.tuple.Pair;

public class NumericalCrowdRating implements Serializable {
	private static final long							serialVersionUID	= 3323980596192046055L;
	private final List<Pair<Integer, ? extends Number>>	judgments;

	public NumericalCrowdRating(int workerId, double judgment) {
		this.judgments = Arrays.asList(Pair.of(workerId, judgment));
	}

	public NumericalCrowdRating(List<Pair<Integer, ? extends Number>> judgments) {
		this.judgments = judgments;
	}

	public List<Pair<Integer, ? extends Number>> getJudgments() {
		return judgments;
	}

	public double getMean() {
		return judgments.stream().mapToDouble(x -> x.second().doubleValue())
				.average().getAsDouble();
	}

	public double getMean(int maxSamples) {
		return judgments.stream()
				.sorted((x, y) -> Integer.compare(x.hashCode(), y.hashCode()))
				.limit(maxSamples).mapToDouble(x -> x.second().doubleValue())
				.average().getAsDouble();
	}

	public double getVariance() {
		final double mean = getMean();
		return judgments.stream()
				.mapToDouble(x -> x.second().doubleValue() - mean)
				.map(x -> x * x).average().getAsDouble();
	}

	public Pair<Double, Double> sampleMeanPair(int size1, int size2) {
		final List<Pair<Integer, ? extends Number>> j1 = judgments.stream()
				.collect(DataUtils.reservoirSamplingCollector(size1));
		final List<Pair<Integer, ? extends Number>> j2 = judgments.stream()
				.filter(j -> !j1.contains(j))
				.collect(DataUtils.reservoirSamplingCollector(size2));
		return Pair.of(j1.stream().mapToDouble(p -> p.second().doubleValue())
				.average().getAsDouble(),
				j2.stream().mapToDouble(p -> p.second().doubleValue())
						.average().getAsDouble());
	}
}