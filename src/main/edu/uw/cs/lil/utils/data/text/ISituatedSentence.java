package edu.uw.cs.lil.utils.data.text;

public interface ISituatedSentence<T extends ISituatedToken<? extends ISentence<T>>, STATE>
		extends ISentence<T> {
	STATE getState();
}
