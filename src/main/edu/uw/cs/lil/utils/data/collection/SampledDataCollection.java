package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.stream.Stream;

public class SampledDataCollection<DI> implements IDataCollection<DI> {
	private final IDataCollection<DI>	data;
	private final int					size;

	public SampledDataCollection(IDataCollection<DI> data, double ratio) {
		this.data = data;
		this.size = (int) Math.ceil(data.size() * ratio);
	}

	@Override
	public Iterator<DI> iterator() {
		return stream().iterator();
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public Stream<DI> stream() {
		return data.stream()
				.sorted((x, y) -> Integer.compare(x.hashCode(), y.hashCode()))
				.limit(size);
	}
}