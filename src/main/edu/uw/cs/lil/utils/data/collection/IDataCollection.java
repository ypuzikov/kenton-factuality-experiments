package edu.uw.cs.lil.utils.data.collection;

import java.util.Iterator;
import java.util.stream.Stream;

public interface IDataCollection<DI> extends Iterable<DI> {
	@Override
	default Iterator<DI> iterator() {
		return stream().iterator();
	}

	int size();

	Stream<DI> stream();
}