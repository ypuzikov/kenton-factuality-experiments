package edu.uw.cs.lil.utils.data.text;

import java.io.Serializable;

public interface IToken extends Serializable {
	public String getAfter();

	public String getBefore();

	public int getCharEnd();

	public int getCharStart();

	public String getDependencyType();

	public int getGovernor();

	public int getIndex();

	public String getTag();

	public String getText();
}
