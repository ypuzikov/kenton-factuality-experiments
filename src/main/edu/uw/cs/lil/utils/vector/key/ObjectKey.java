package edu.uw.cs.lil.utils.vector.key;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import edu.uw.cs.lil.utils.DataUtils;

public class ObjectKey implements ISparseKey {
	private static final long			serialVersionUID	= -446824513030985258L;
	private final ImmutableList<Object>	objects;
	private final int					hashCode;

	private ObjectKey(Object... objects) {
		this.objects = Arrays.stream(objects).collect(
				DataUtils.immutableListCollector());
		this.hashCode = Objects.hash(this.objects);
	}

	public static ObjectKey of(Object... objects) {
		return new ObjectKey(objects);
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}
		if (other instanceof ObjectKey) {
			return Objects.equals(this.objects, ((ObjectKey) other).objects);
		} else {
			return false;
		}
	}

	@Override
	public Object getStructure() {
		return objects;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public int lexicalSize() {
		return 0;
	}

	@Override
	public Stream<String> lexicalStream() {
		return Stream.empty();
	}

	@Override
	public String toString() {
		return objects
				.stream()
				.map(obj -> obj instanceof java.lang.Class<?> ? ((java.lang.Class<?>) objects
						.get(0)).getSimpleName() : obj.toString())
				.collect(Collectors.joining(","));
	}
}
