package edu.uw.cs.lil.utils.vector.key;

import java.io.Serializable;
import java.util.stream.Stream;

public interface ISparseKey extends Serializable {
	Object getStructure();

	int lexicalSize();

	Stream<String> lexicalStream();
}
