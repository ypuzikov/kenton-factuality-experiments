package edu.uw.cs.lil.utils.vector.key;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.collect.ImmutableList;

import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.data.tuple.Pair;

public class CompositeKey implements ISparseKey {
	private static final long serialVersionUID = 3055820528411438312L;
	private final ImmutableList<ISparseKey>							components;
	private final int												hashCode;
	private final Pair<Class<CompositeKey>, ImmutableList<Object>>	structure;
	private final int												lexicalSize;

	private CompositeKey(ISparseKey... components) {
		this.components = Arrays.stream(components).collect(
				DataUtils.immutableListCollector());
		this.structure = Pair.of(
				CompositeKey.class,
				this.components.stream().map(ISparseKey::getStructure)
						.collect(DataUtils.immutableListCollector()));
		this.hashCode = Objects.hash(this.components);
		this.lexicalSize = this.components.stream()
				.mapToInt(ISparseKey::lexicalSize).sum();
	}

	public static CompositeKey of(ISparseKey... components) {
		return new CompositeKey(components);
	}

	@Override
	public boolean equals(Object other) {
		if (other instanceof CompositeKey) {
			return Objects.equals(this.components,
					((CompositeKey) other).components);
		} else {
			return false;
		}
	}

	public List<ISparseKey> getComponents() {
		return components;
	}

	@Override
	public Object getStructure() {
		return structure;
	}

	@Override
	public int hashCode() {
		return hashCode;
	}

	@Override
	public int lexicalSize() {
		return lexicalSize;
	}

	@Override
	public Stream<String> lexicalStream() {
		return components.stream().flatMap(ISparseKey::lexicalStream);
	}

	@Override
	public String toString() {
		return "("
				+ components.stream().map(Object::toString)
						.collect(Collectors.joining(",")) + ")";
	}
}
