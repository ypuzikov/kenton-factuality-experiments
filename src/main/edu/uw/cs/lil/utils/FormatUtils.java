package edu.uw.cs.lil.utils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.apache.commons.lang.StringUtils;

import edu.uw.cs.lil.utils.vector.ISparseVector;

public class FormatUtils {
	private FormatUtils() {
	}

	public static String formatFeatureWeights(ISparseVector features,
			ISparseVector weights) {
		return formatFeatureWeights(features, weights, features.size());
	}

	public static String formatFeatureWeights(ISparseVector features,
			ISparseVector weights, int maxWeights) {
		return String.format(
				"{%s}",
				features.stream()
						.sorted((x, y) -> Double.compare(
								Math.abs(weights.get(y.first()) * y.second()),
								Math.abs(weights.get(x.first()) * y.second())))
						.limit(maxWeights)
						.map(p -> String.format("%s=%.3f(%.3f)", p.first(),
								weights.get(p.first()), p.second()))
						.collect(Collectors.joining(",")));
	}

	public static String formatMatrix(double[][] m) {
		return "["
				+ Arrays.stream(m).map(FormatUtils::formatVector)
						.collect(Collectors.joining(",\n")) + "]";
	}

	public static String formatVector(double[] v) {
		return "["
				+ Arrays.stream(v).mapToObj(x -> String.format("%f", x))
						.collect(Collectors.joining(",")) + "]";
	}

	public static String formatWeights(ISparseVector w) {
		return formatWeights(w, w.size());
	}

	public static String formatWeights(ISparseVector w, int maxWeights) {
		return String.format(
				"Top %d features:\n%s",
				maxWeights,
				w.stream()
						.sorted((x, y) -> Double.compare(Math.abs(y.second()),
								Math.abs(x.second()))).limit(maxWeights)
						.map(p -> p.first() + ": " + p.second())
						.collect(Collectors.joining("\n")));
	}

	public static <X extends Comparable<X>, Y extends Comparable<Y>> String plotConfusionMatrix(
			Map<X, Map<Y, AtomicInteger>> confusionMatrix) {
		final StringBuffer sb = new StringBuffer();
		final List<X> xAxis = confusionMatrix.keySet().stream().distinct()
				.sorted().collect(Collectors.toList());
		final List<Y> yAxis = confusionMatrix.values().stream()
				.flatMap(m -> m.keySet().stream()).distinct().sorted()
				.collect(Collectors.toList());
		final int cellWidth = Math.max("pred".length(), Math.max(
				xAxis.stream().mapToInt(c -> c.toString().length()).max()
						.getAsInt(),
				confusionMatrix.values().stream()
						.flatMap(v -> v.values().stream())
						.mapToInt(count -> (int) (Math.log10(count.get()) + 1))
						.max().getAsInt())) + 1;
		// Header row
		sb.append(formatConfusionMatrixObject("", cellWidth));
		sb.append(formatConfusionMatrixObject("x", cellWidth));
		for (final X x : xAxis) {
			sb.append(formatConfusionMatrixObject(x, cellWidth));
		}
		sb.append("\n");

		// Underline row
		sb.append(formatConfusionMatrixObject("y", cellWidth));
		IntStream.range(0, xAxis.size() + 1).forEach(
				i -> sb.append(formatConfusionMatrixObject("-", cellWidth)));
		sb.append("\n");

		for (final Y y : yAxis) {
			// Body row
			sb.append(formatConfusionMatrixObject(y, cellWidth));
			sb.append(formatConfusionMatrixObject("|", cellWidth));
			for (final X x : xAxis) {
				sb.append(formatConfusionMatrixObject(confusionMatrix.get(x)
						.get(y), cellWidth));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	public static <X extends Comparable<X>, Y extends Comparable<Y>> String plotLatexConfusionMatrix(
			Map<X, Map<Y, AtomicInteger>> confusionMatrix, String xLabel,
			String yLabel) {
		final List<X> xAxis = confusionMatrix.keySet().stream().distinct()
				.sorted().collect(Collectors.toList());
		final List<Y> yAxis = confusionMatrix.values().stream()
				.flatMap(m -> m.keySet().stream()).distinct().sorted()
				.collect(Collectors.toList());
		final StringBuffer sb = new StringBuffer();
		sb.append(String.format("\\begin{tabular}{|*{%d}{c|}}\\cline{3-%d}\n",
				xAxis.size() + 2, xAxis.size() + 2));
		sb.append(String
				.format("\\multicolumn{2}{c|}{} & \\multicolumn{%d}{c|}{%s}\\\\\\cline{3-%d}\n",
						xAxis.size(), xLabel, xAxis.size() + 2));
		sb.append(String.format(
				"\\multicolumn{2}{c|}{} & %s\\\\\\hline\n",
				yAxis.stream().map(Object::toString)
						.collect(Collectors.joining("&"))));
		sb.append(String.format(
				"\\multirow{%d}{*}{\\rotatebox[origin=c]{90}{%s}}\n",
				yAxis.size(), yLabel));

		int count = 0;
		for (final Y y : yAxis) {
			count++;
			sb.append("&" + y.toString() + "&");
			sb.append(xAxis.stream().map(x -> confusionMatrix.get(x).get(y))
					.map(Object::toString).collect(Collectors.joining("&")));
			if (count < yAxis.size()) {
				sb.append(String.format("\\\\\\cline{2-%d}\n", xAxis.size() + 2));
			} else {
				sb.append(String.format("\\\\\\hline\n", xAxis.size() + 2));
			}

		}
		sb.append("\\end{tabular}\n");
		return sb.toString();
	}

	private static String formatConfusionMatrixObject(Object obj, int cellWidth) {
		return StringUtils.rightPad(obj.toString(), cellWidth);
	}
}