package edu.uw.cs.lil.exp.creator;

import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;

import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.utils.ConfigUtils;

public class StringFileCreator implements IResourceCreator<String> {
	@Override
	public String create(HierarchicalConfiguration config,
			IResourceRepository resourceRepo,
			IResourceCreatorRepository resourceCreatorRepo) {
		try {
			return Files.lines(ConfigUtils.getFile("path", config).toPath())
					.collect(Collectors.joining("\n"));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String type() {
		return "string.file";
	}
}
