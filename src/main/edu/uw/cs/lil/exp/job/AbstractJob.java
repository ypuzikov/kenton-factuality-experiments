package edu.uw.cs.lil.exp.job;

import java.util.Set;

public abstract class AbstractJob implements IJob {
	private final Set<String>	dependencies;
	private final String		id;

	public AbstractJob(String id, Set<String> dependencies) {
		this.id = id;
		this.dependencies = dependencies;
	}

	@Override
	public Set<String> getDependencies() {
		return dependencies;
	}

	@Override
	public String getId() {
		return id;
	}
}
