package edu.uw.cs.lil.exp.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ResourceRepository implements IResourceRepository {
	private final Map<String, Object>	resourceMap;

	public ResourceRepository() {
		this.resourceMap = new HashMap<>();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T getResource(String id) {
		return (T) Optional.ofNullable(resourceMap.get(id)).orElseThrow(
				() -> new IllegalArgumentException(
						"Unable to find existing resource: " + id));
	}

	@Override
	public void put(String id, Object resource) {
		this.resourceMap.put(id, resource);
	}

	@Override
	public String toString() {
		return resourceMap.toString();
	}
}
