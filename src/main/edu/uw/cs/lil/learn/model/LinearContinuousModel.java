package edu.uw.cs.lil.learn.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.learn.featureset.BiasFeatureSet;
import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;
import edu.uw.cs.lil.utils.vector.key.ObjectKey;

public class LinearContinuousModel<V> implements
		IModel<V, Double, IMutableSparseVector> {
	public static final Logger			log	= Logger.getLogger(LinearContinuousModel.class);
	private final IMutableSparseVector	theta;
	private final Map<V, ISparseVector>	cache;
	private final List<IFeatureSet<V>>	featureSets;

	public LinearContinuousModel(boolean cached) {
		this(new ArrayList<>(), cached);
	}

	public LinearContinuousModel(List<IFeatureSet<V>> featureSets,
			boolean cached) {
		this.theta = new SparseVector();
		this.featureSets = featureSets;
		this.cache = cached ? new ConcurrentHashMap<>() : null;
	}

	public void addFeatureSet(IFeatureSet<V> fs) {
		this.featureSets.add(fs);
	}

	@Override
	public ISparseVector getFeatures(V sample) {
		if (cache != null) {
			return cache.computeIfAbsent(sample,
					key -> featureSets.stream().map(f -> f.getFeatures(key))
							.collect(SparseVector.sumCollector()));
		} else {
			return featureSets.stream().map(f -> f.getFeatures(sample))
					.collect(SparseVector.sumCollector());
		}
	}

	@Override
	public IMutableSparseVector getParams() {
		return theta;
	}

	@Override
	public Double getResult(V sample) {
		return getFeatures(sample).dotProduct(theta)
				+ theta.get(ObjectKey.of(BiasFeatureSet.class));
	}
}
