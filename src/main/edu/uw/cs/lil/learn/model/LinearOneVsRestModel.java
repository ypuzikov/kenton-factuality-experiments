package edu.uw.cs.lil.learn.model;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import edu.uw.cs.lil.learn.featureset.IFeatureSet;
import edu.uw.cs.lil.utils.DataUtils;
import edu.uw.cs.lil.utils.data.DefaultHashMap;
import edu.uw.cs.lil.utils.vector.ISparseVector;

public class LinearOneVsRestModel<V, A> implements
		IModel<V, A, Map<A, LinearBinaryModel<V>>> {
	private final Map<A, LinearBinaryModel<V>>	models;

	public LinearOneVsRestModel(boolean cached) {
		this(Collections.emptyList(), cached);
	}

	public LinearOneVsRestModel(List<IFeatureSet<V>> featureSets, boolean cached) {
		this.models = new DefaultHashMap<>(a -> new LinearBinaryModel<>(
				featureSets, cached));
	}

	@Override
	public ISparseVector getFeatures(V sample) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<A, LinearBinaryModel<V>> getParams() {
		return models;
	}

	@Override
	public A getResult(V sample) {
		return models
				.entrySet()
				.stream()
				.collect(
						DataUtils.argmaxCollector(entry -> entry.getValue()
								.getBaseModel().getResult(sample))).get()
				.getKey();
	}
}
