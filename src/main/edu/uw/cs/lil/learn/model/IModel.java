package edu.uw.cs.lil.learn.model;

import edu.uw.cs.lil.utils.vector.ISparseVector;

public interface IModel<V, A, P> {
	ISparseVector getFeatures(V sample);

	P getParams();

	A getResult(V sample);
}
