package edu.uw.cs.lil.learn.model;

import edu.uw.cs.lil.utils.vector.IMutableSparseVector;
import edu.uw.cs.lil.utils.vector.ISparseVector;
import edu.uw.cs.lil.utils.vector.SparseVector;

public abstract class AbstractDeterministicModel<V, A> implements
		IModel<V, A, IMutableSparseVector> {
	final IMutableSparseVector	dummyParams	= SparseVector.of();

	@Override
	public ISparseVector getFeatures(V sample) {
		return SparseVector.EMPTY;
	}

	@Override
	public IMutableSparseVector getParams() {
		return dummyParams;
	}
}
