package edu.uw.cs.lil.learn.learner;

import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.external.svm.SVMClassification;
import edu.uw.cs.lil.events.external.svm.SVMClassification.Sample;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class SVMClassificationLearner<V> implements ILinearLearner<V, Boolean> {
	public static final Logger	log	= Logger.getLogger(SVMClassificationLearner.class);
	private final double		cost;

	public SVMClassificationLearner(double cost) {
		this.cost = cost;
	}

	@Override
	public void train(IModel<V, Boolean, IMutableSparseVector> model,
			IDataCollection<V> data, Function<V, Boolean> labeler,
			ToDoubleFunction<V> varianceLabeler) {
		final List<Sample> svmSamples = data
				.stream()
				.map(sample -> Sample.of(labeler.apply(sample), cost,
						model.getFeatures(sample)))
				.collect(Collectors.toList());
		model.getParams().clear();
		model.getParams().addToSelf(SVMClassification.learnModel(svmSamples));
	}

	public static class Creator<V> implements
			IResourceCreator<SVMClassificationLearner<V>> {
		@Override
		public SVMClassificationLearner<V> create(
				HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new SVMClassificationLearner<>(config.getDouble("cost"));
		}

		@Override
		public String type() {
			return "learner.svm.classification";
		}
	}
}
