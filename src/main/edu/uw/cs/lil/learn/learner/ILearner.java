package edu.uw.cs.lil.learn.learner;

import java.util.function.Function;
import java.util.function.ToDoubleFunction;

import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;

public interface ILearner<V, A, P> {
	default void train(IModel<V, A, P> model, IDataCollection<V> data,
			Function<V, A> labeler) {
		train(model, data, labeler, null);
	}

	void train(IModel<V, A, P> model, IDataCollection<V> data,
			Function<V, A> labeler, ToDoubleFunction<V> varianceLabeler);
}
