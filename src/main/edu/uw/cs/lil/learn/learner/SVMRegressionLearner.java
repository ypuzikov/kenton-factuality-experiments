package edu.uw.cs.lil.learn.learner;

import java.util.List;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.log4j.Logger;

import edu.uw.cs.lil.events.external.svm.SVMRegression;
import edu.uw.cs.lil.events.external.svm.SVMRegression.Sample;
import edu.uw.cs.lil.exp.creator.IResourceCreator;
import edu.uw.cs.lil.exp.creator.IResourceCreatorRepository;
import edu.uw.cs.lil.exp.repository.IResourceRepository;
import edu.uw.cs.lil.learn.model.IModel;
import edu.uw.cs.lil.utils.data.collection.IDataCollection;
import edu.uw.cs.lil.utils.vector.IMutableSparseVector;

public class SVMRegressionLearner<V> implements ILinearLearner<V, Double> {
	public static final Logger			log	= Logger.getLogger(SVMRegressionLearner.class);
	private final Function<V, Double>	costLabeler;
	private final double				epsilon;

	public SVMRegressionLearner(double cost, double epsilon) {
		this(x -> cost, epsilon);
	}

	public SVMRegressionLearner(Function<V, Double> costLabeler, double epsilon) {
		this.costLabeler = costLabeler;
		this.epsilon = epsilon;
	}

	@Override
	public void train(IModel<V, Double, IMutableSparseVector> model,
			IDataCollection<V> data, Function<V, Double> labeler,
			ToDoubleFunction<V> varianceLabeler) {
		final List<Sample> svmSamples = data
				.stream()
				.map(sample -> Sample.of(labeler.apply(sample),
						costLabeler.apply(sample), model.getFeatures(sample)))
				.collect(Collectors.toList());
		model.getParams().clear();
		model.getParams().addToSelf(
				SVMRegression.learnModel(svmSamples, epsilon));
	}

	public static class Creator<V> implements
			IResourceCreator<SVMRegressionLearner<V>> {
		@Override
		public SVMRegressionLearner<V> create(HierarchicalConfiguration config,
				IResourceRepository resourceRepo,
				IResourceCreatorRepository resourceCreatorRepo) {
			return new SVMRegressionLearner<>(config.getDouble("cost"),
					config.getDouble("epsilon"));
		}

		@Override
		public String type() {
			return "learner.svm.regression";
		}
	}
}
